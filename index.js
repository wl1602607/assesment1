const express = require("express");
const app = express();
const fs = require("fs").promises;
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send(`
      <form action="/register" method="POST">
          <div>
              <label for="fullname">Full Name:</label>
              <input type="text" id="fullname" name="fullname" required>
          </div>
          <div>
              <label for="email">Email:</label>
              <input type="email" id="email" name="email" required>
          </div>
          <div>
              <label for="course">Course:</label>
              <select id="course" name="course" required>
              <option value="Tamil">Tamil</option>
              <option value="English">English</option>
                  <option value="Math">Math</option>
                  <option value="Science">Science</option>
                  <option value="Social">Social</option>
              </select>
          </div>
          <button type="submit">Register Course</button>
      </form>`);
});
app.post("/register", async (req, res) => {
  const { fullname, email, course } = req.body;
  const data = { fullname, email, course };

  try {
    let existingData = [];
    try {
      const fileContent = await fs.readFile("temp.json", "utf8");
      existingData = JSON.parse(fileContent);
    } catch (error) {
      // If the file doesn't exist yet, continue with an empty array
    }
    existingData.push(data);
    await fs.writeFile("temp.json", JSON.stringify(existingData, null, 2));
    res.send("Registration successful!");
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(5000, () => {
  console.log("Server is running on port 5000");
});
