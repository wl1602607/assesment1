STEPS TO SETUP CI/CD PIPELINE
step 1: Go to current repository and create a new file as .gitlab-ci.yml
step 2: Give stages as build and deploy.
step 3: In build give image as node for nodejs.
other wise it will convert in to ruby and it will run so it takes more time to execute.
So for my convience I gave image as node.
step 4: In script give npm install to download required dependencies for that program.
step 5: Give artifacts to commen folder of the project.
Here node_modules and package-lock.json is present.
step 6: Then in deploy give image and "node index.js" for run the program
and "/dev/null 2>&1 &" for end the program because here it is web based application so we end the program.

TIPS:
For troubleshooting check the program of .gitlab-ci.yml is valid and check console output and rectify the error.
